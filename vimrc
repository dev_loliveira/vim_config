set nohlsearch

set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

set foldmethod=indent   "fold based on indent
set foldnestmax=10      "deepest fold is 10 levels
set nofoldenable        "dont fold by default
set foldlevel=1         "this is just what i use

set incsearch " Utilizado para efetuar o highligh
              " a medida em que os critérios de busca
              " forem sendo digitados.
set hlsearch
set background=dark


set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Chamando pathogen
execute pathogen#infect()

" let Vundle manage Vundle
Bundle 'gmarik/vundle'
Bundle 'scrooloose/nerdtree'
map <F2> :NERDTreeToggle<CR>
" " The bundles you install will be listed here
filetype plugin indent on

" " Restringindo o numero de caracteres por linha
augroup vimrc_autocmds
    autocmd!
    " highlight characters past column 120
    autocmd FileType python highlight Excess ctermbg=DarkGrey guibg=Black
    autocmd FileType python match Excess /\%120v.*/
    autocmd FileType python set nowrap
augroup END

" " Utilizando powerline
Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ 9
set laststatus=2

" "
Bundle 'klen/python-mode'

let g:jedi#popup_on_dot=0
let g:pymode_lint_write=0
let g:pymode_utils_whitespaces = 0
let g:pymode_virtualenv = 0
let NERDTreeIgnore = ['\.pyc$']
map <leader>j :RopeGotoDefinition

" " Custom functions for vim
function! TemplatePythonPudb()
    r~/.vim/templates/python/pudb.txt
endfunction
nmap <leader>pudb :call TemplatePythonPudb()<CR>

hi Visual cterm=NONE ctermbg=Black ctermfg=White

" Permitir alterar de tabs utilizando a tecla TAB
set showtabline=2
noremap <C-n> :tabnew 
noremap <Tab> :tabnext<CR>
cnoremap ls<tab> <C-d>
noremap <C-V> <C-v>

nmap tn :tabnew 
nmap tc :q<CR>
nmap <Leader>spl :vsplit<CR>
